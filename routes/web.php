<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Http\Middleware\Authenticate;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/new-login',function(){
    return view('newLogin');
});
Route::get('/new-register',function(){
    return view('newRegister');
});
Route::get('/new-forget',function(){
    return view('newForget');
});
Route::get('/', function () {
    return view('welcome');
});
Route::get('/user','UserController@index');

Auth::routes();
Route::middleware('auth')->group(function(){
 Route::get('/products','ProductController@index')->name('product.index');
Route::get('/products/create','ProductController@create');
Route::get('/products/{id}/edit','ProductController@edit');
Route::get('/products/{proid}/delete','ProductController@delete')->name('product.delete');
Route::patch('/products/{proid}/update','ProductController@update')->name('product.update');
Route::get('/products/{id}/show','ProductController@show');
Route::post('/products/create','ProductController@store');   
});
Route::get('/home', 'HomeController@index')->name('home');

// to call the function directly from route

//Route::post('/upload', function(Request $request){
//    // to see the properties of the uploaded image
//   // dd($request->file('myimage'));
//    //or we can use
//   // dd($request->myimage);
//    // to check the file has loaded or not
//    //dd($request->hasFile('myimage'));
//    //it will first create the images folder in the storage folder and then save the image
//  //  dd($request->myimage->store('images'));
//    // it will make images folder in public folder of storage 
//    dd($request->myimage->store('images','public'));
//    return('sdcsd');
//    
//});

//  the same above we can call from controller
Route::post('/upload', 'UserController@uploadImage');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
