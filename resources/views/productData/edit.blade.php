@extends('layouts.app')
   
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2><center>Edit Product</center></h2>
            </div>
            <div class="pull-right">
                <a class=" my-3 mx-3 btn btn-primary" href="/products"> Back</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('product.update',$productData->id) }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ method_field('PATCH') }}
   
      
              <div class="flex justify-center">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="  mx-5 form-group">
                    <strong>Name:</strong>
                    <input type="text" name="product_name" value="{{ $productData->product_name }}" class="form-control" placeholder="Name">
                </div>
            </div>
              </div>
             <div class="flex justify-center">
                <div class="col-xs-12 col-sm-12 col-md-12">
                     <div class=" mx-5 form-group">
                          <img src="{{ asset('/storage/images/'.$productData->product_image)}}"  width="40"/>
                         <strong class=" mx-5">Image:</strong>
                                 @csrf
                                 <input type="file" name="myimage"/>
                     </div>
                </div>
             </div>
         <div class="flex justify-center">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class=" mx-5 form-group">
                    <strong>Details:</strong>
                    <input type="text" class="form-control" name="product_details" value ="{{ $productData->product_details }}" placeholder="Details">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
       
   
    </form>
@endsection 