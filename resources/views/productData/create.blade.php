

@extends('layouts.app')
  
@section('content')
 
    <div class=" flex justify-center   ">
        
        <h2><center>Add New Product</center></h2>
       
       
            <a class="btn btn-primary" href="/products"> Back</a>
       
    </div>

   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   @include('layouts.flash')
<form action="/products/create" method="POST" enctype="multipart/form-data">
    {{ csrf_field() }}
 
     <div class="flex justify-center">
       <div class="col-xs-12 col-sm-12 col-md-12">
            <div class=" mx-5 form-group row" >
                <strong>Name:</strong>
                <input type="text" name="product_name" class="form-control" placeholder="Name">
            </div>
       </div>
     </div>
     <div class="flex justify-center">
       <div class="col-xs-12 col-sm-12 col-md-12">
            <div class=" mx-5 form-group">
                <strong>Image:</strong>
            
                        @csrf
                        <input type="file" name="myimage"/>
                        
                   
            </div>
        </div>
     </div>
                 <div class="flex justify-center">
            <div class="col-xs-12 col-sm-12 col-md-12">
            <div class=" mx-5 form-group">
                <strong>Details:</strong>
                <input type="text" name="product_details" class="  form-control" placeholder="Details">
            </div>
        </div>
                 </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
  
</form>
@endsection  