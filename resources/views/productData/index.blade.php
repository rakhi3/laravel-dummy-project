 @extends('layouts.app')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb ">
            
            <h2><center>Product List</center></h2>
           
            <div class=" my-5 pull-right ">
                <a class="btn btn-success" href="/products/create"> Create New Product</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('message'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class=" mx-3 table table-bordered">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Image</th>
            <th>Details</th>
            <th width="280px">Action</th>
        </tr>
         <?php $i = 0 ?>
        @foreach ($productData as $pData)
       <?php $i++ ?>
        <tr>
            <td>{{ $i }}</td>
            <td>{{ $pData->product_name }}</td>
            <td>
                @if ($pData->product_image)
            <img src="{{ asset('/storage/images/'.$pData->product_image)}}"  width="40"/>
                                @endif
            </td>
            <td>vfvf{{ $pData->product_name }}</td>
            <td><a class="btn btn-primary" href="{{ route('product.delete',$pData->id) }}">Delete</a>
                <a class="btn btn-primary" href="{{'/products/'.$pData->id.'/edit'}}">Edit</a>
            </td>        
                    
        </tr>
        @endforeach
    </table>
  
  
      
@endsection 
