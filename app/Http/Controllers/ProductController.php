<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   
    public function create()
    {
        return view('productData.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
           $request->validate([
            'product_name' => 'required|max:255',
            'myimage' => 'required',
        ]);
        
               $fileName=$request->myimage->getClientOriginalName(); 
             $request->myimage->storeAs('images',$fileName,'public');
             // to save the file by its original name
            
         
        $data=[
            'product_name' => $request->product_name,
            'product_details' => $request->product_details,
             'product_image' => $fileName,
        ];
         Product::create($data);
       // Product::create($request->all());
   
        return redirect()->back()
                        ->with('message','Product created successfully.');
    }
     
//    /**
//     * Display the specified resource.
//     *
//     * @param  \App\Product  $productData
//     * @return \Illuminate\Http\Response
//     */
    public function show($id)
    {
        $productData = Product::find($id);
        return view('productData.show',compact('productData'));
    }

//    /**
//     * Show the form for editing the specified resource.
//     *
//     * @param  \App\Product  $productData
//     * @return \Illuminate\Http\Response
//     */
    public function edit($id)
    {
        $productData = Product::find($id);
        return view('productData.edit',compact('productData','id'));
    }
//
//    /**
//     * Update the specified resource in storage.
//     *
//     * @param  \Illuminate\Http\Request  $request
//     * @param  \App\Product  $productData
//     * @return \Illuminate\Http\Response
//     */
    public function update(Request $request,Product $proid)
    {
       
        
        $request->validate([
                'product_name' => 'required',
                
         ]);
        $proid->product_name = request('product_name');
        $proid->product_details = request('product_details');
        if($request->hasFile('myimage'))
        {
         $fileName=$request->myimage->getClientOriginalName(); 
            if($proid->product_image){
                Storage::delete('/public/images/'.$proid->product_image);
            }
             $request->myimage->storeAs('images',$fileName,'public');
             $proid->product_image =$fileName;
        }    
             $proid->save();
                
        $proid->update($request->all());
  
        return redirect(route('product.index'))
                        ->with('message','Product updated successfully');
    }
//
//    /**
//     * Remove the specified resource from storage.
//     *
//     * @param  \App\Product  $productData
//     * @return \Illuminate\Http\Response
//     */
    public function delete(Product $proid)
    {
         if($proid->product_image){
                Storage::delete('/public/images/'.$proid->product_image);
            }
        Product::find($proid->id)->delete();
        
        return redirect()->route('product.index')
                        ->with('message','Product deleted successfully');
    }
     public function index(){
          $productData = Product::all();
  
        return view('productData.index')
            ->with(['productData'=>$productData]);
         
         // return view('productData.index');
     }
}
