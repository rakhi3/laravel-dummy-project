<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
class UserController extends Controller
{
//    public function uploadImage(Request $request ) {
//        if($request->hasFile('myimage'))
//        {
//            //getClientOriginalName() this function is define in upladedfile of illuminate
//            //it will give you the name of the uploaded file
//            $fileName=$request->myimage->getClientOriginalName();
//            if(auth()->user()->image){
//                Storage::delete('/public/images/'.auth()->user()->image);
//            }
//            
//             // to save the file by its original name
//        $request->myimage->storeAs('images',$fileName,'public');
//        // to save the file in image field of users table in database of logined user 
//        auth()->user()->update(['image'=>$fileName]);
//        }
//       
//    return redirect()->back();
//    }
    
    public function uploadImage(Request $request ) {
        if($request->hasFile('myimage'))
        {
            User::uploadImage($request->myimage);
           // $request->session()->flash('message','Image uploaded');
           //  return redirect()->back();
            //or
            return redirect()->back()->with('message','Image uploaded');
        }
        // $request->session()->flash('error','Image not uploaded');
        return redirect()->back()->with('error','Image not uploaded');
    }  
    
    public function index(){
        $data =[
            'name' =>'Test1',
            'email'=>'test1@gmail.com',
            'password' =>'password',
        ];
        User::create($data);
//       $user= new User();
//       $user->name='Rakhi';
//       $user->email='rakhi@gmail.com';
//       $user->password=bcrypt('password');
//       $user->save();
//       
//       
       // dd($user);
        
        //show all users
       $user=User::all();
       return $user;
        
    //    User:: where('id',1)->delete();
        
     //   User:: where('id',1)->update(['name'=>'Rakhi agarwal']);
        return view('myhomepage');
       // return 'I am in user controller';
    }
//
}
