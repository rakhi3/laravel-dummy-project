<?php

namespace App;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
//    public function setPasswordAttribute($password)
//    {
//        $this->attributes['Password']=bcrypt($password);
//    }
    
    public static function uploadImage($image) {
         $fileName=$image->getClientOriginalName();
           
            auth()->user()->oldDeleteImage();
             // to save the file by its original name
        $image->storeAs('images',$fileName,'public');
        // to save the file in image field of users table in database of logined user 
        auth()->user()->update(['image'=>$fileName]);
        
    }
    
    protected function oldDeleteImage()
    {
         if(auth()->user()->image){
                Storage::delete('/public/images/'.auth()->user()->image);
            }
    }
}
